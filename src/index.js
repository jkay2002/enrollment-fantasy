import style from "./main.scss";
import Confetti from './js/confetti';
import WelcomeComponent from './js/components/welcome/welcome.component';
import OverviewComponent from './js/components/overview/overview.component';
import HeaderComponent from './js/components/header/header.component';
import TranscriptOverviewComponent from './js/components/transcript-overview/transcript-overview.component';
import TranscriptEndComponent from './js/components/transcript-end/transcript-end.component';
import TranscriptQuestionTranscriptsComponent from './js/components/transcript-question-transcripts/transcript-question-transcripts.component';
import TranscriptQuestionCertificationsComponent from './js/components/transcript-question-certifications/transcript-question-certifications.component';
import TranscriptAdditionComponent from './js/components/transcript-addition/transcript-addition.component';
import TranscriptCertificationAdditionComponent from './js/components/transcript-certification-addition/transcript-certification-addition.component';
import FinancialAidOverviewComponent from './js/components/financial-aid-overview/financial-aid-overview.component';
import FinancialAidPaymentsComponents from './js/components/financial-aid-payments/financial-aid-payments.components';
import FinancialAidSetPaymentsComponents from './js/components/financial-aid-set-payments/financial-aid-set-payments.components';
import FinancialAidEndComponent from './js/components/financial-aid-end/financial-aid-end.component';
import FileHubComponent from './js/components/file-hub/file-hub.component';
import FileHubUploadComponent from './js/components/file-hub-upload/file-hub-upload.component';
import StartDateOverviewComponent from './js/components/start-date-overview/start-date-overview.component';
import StartDateChangeComponent from './js/components/start-date-change/start-date-change.component';
import StartDateEndComponent from './js/components/start-date-end/start-date-end.component';
import ReadinessAssessmentOverviewComponent from './js/components/readiness-assessment-overview/readiness-assessment-overview.component';
import ReadinessAssessmentEndComponent from './js/components/readiness-assessment-end/readiness-assessment-end.component';
import ProgramOverviewComponent from './js/components/program-overview/program-overview.component';
import ProgramChangeComponent from './js/components/program-change/program-change.component';
import ProgramEndComponent from './js/components/program-end/program-end.component';
import IntakeInterviewOverviewComponent from './js/components/intake-interview-overview/intake-Interview-overview.component';
import IntakeInterviewEndComponent from './js/components/intake-interview-end/intake-interview-end.component';

let components = [
	{ element: 'header-component', componentName: HeaderComponent },
	{ element: 'welcome-component', componentName: WelcomeComponent },
	{ element: 'overview-component', componentName: OverviewComponent },
	{ element: 'transcript-overview-component', componentName: TranscriptOverviewComponent },
	{ element: 'transcript-end-component', componentName: TranscriptEndComponent },
	{ element: 'transcript-question-transcripts-component', componentName: TranscriptQuestionTranscriptsComponent },
	{ element: 'transcript-question-certifications-component', componentName: TranscriptQuestionCertificationsComponent },
	{ element: 'transcript-addition-component', componentName: TranscriptAdditionComponent },
	{ element: 'transcript-certification-addition-component', componentName: TranscriptCertificationAdditionComponent },
	{ element: 'financial-aid-overview-component', componentName: FinancialAidOverviewComponent },
	{ element: 'financial-aid-payments-component', componentName: FinancialAidPaymentsComponents },
	{ element: 'financial-aid-set-payments-component', componentName: FinancialAidSetPaymentsComponents },
	{ element: 'financial-aid-end-component', componentName: FinancialAidEndComponent },
	{ element: 'file-hub-component', componentName: FileHubComponent },
	{ element: 'file-hub-upload-component', componentName: FileHubUploadComponent },
	{ element: 'start-date-component', componentName: StartDateOverviewComponent },
	{ element: 'start-date-change-component', componentName: StartDateChangeComponent },
	{ element: 'start-date-end-component', componentName: StartDateEndComponent },
	{ element: 'readiness-assessment-component', componentName: ReadinessAssessmentOverviewComponent },
	{ element: 'readiness-assessment-end-component', componentName: ReadinessAssessmentEndComponent },
	{ element: 'program-component', componentName: ProgramOverviewComponent },
	{ element: 'program-change-component', componentName: ProgramChangeComponent },
	{ element: 'program-end-component', componentName: ProgramEndComponent },
	{ element: 'intake-interview-component', componentName: IntakeInterviewOverviewComponent },
	{ element: 'intake-interview-end-component', componentName: IntakeInterviewEndComponent },
];

components.forEach(component => {
	window.customElements.define(component.element, component.componentName);
});

window.showSideNav = () => {
	//alert(parseInt(document.getElementById('side-nav').style.right));
	if (parseInt(document.getElementById('side-nav').style.right) < 0 ) {
		//document.getElementById('side-nav').classList.remove('d-none');
		document.getElementById('side-nav-toucher').classList.remove('d-none');

		//document.querySelector('.wrapper').classList.add('move-wrapper-in');

		TweenLite.to(document.getElementById('side-nav'), 0.2, {
			right: 0,
			ease: Power0.easeNone,
			onStart: function() {
				document.getElementById('side-nav').classList.add('shadow');
			}
		});

		TweenLite.to(document.querySelector('.wrapper'), 0.2, {
			right: 280,
			ease: Power0.easeNone,
		});
	} else {
		TweenLite.to(document.getElementById('side-nav'), 0.2, {
			right: -280,
			ease: Power0.easeNone,
			onComplete: function() {
				document.getElementById('side-nav').classList.remove('shadow');
			}
		});

		TweenLite.to(document.querySelector('.wrapper'), 0.2, {
			right: 0,
			ease: Power0.easeNone,
			onComplete: function() {
				document.getElementById('side-nav-toucher').classList.add('d-none');
			}
		});
	}
};

document.querySelectorAll('button.button.save').forEach( element => {
	element.addEventListener('click', (e) => {
		console.log(e.srcElement.name);
		let id = e.srcElement.name;
		//btn-transcript-question-transcripts
		document.getElementById(id).removeAttribute('disabled');
	});
});

document.querySelectorAll('.delete-icon').forEach( element => {
	element.addEventListener('click', (e) => {
		let response = confirm('Are you sure you want to delete?');
		if (response) {
			console.log('yes');
		} else {
			console.log('no');
		}
	});
});

function radioButton (e) {

	let id, oldElement, newElement;

	console.log(e.srcElement.name);
	switch (e.srcElement.value.toLowerCase()) {
		case 'yes':
			id = e.srcElement.name;
			document.getElementById(id).removeAttribute('disabled');

			oldElement = document.getElementById(id);
			newElement = oldElement.cloneNode(true);
			oldElement.parentNode.replaceChild(newElement, oldElement);

			switch (id) {
				case 'btn-continue-transcript-question-transcripts':
					document.getElementById(id).addEventListener('click', event => {
						nextSlide('transcript-addition');
					});
					break;
				case 'btn-continue-transcript-question-certification':
					document.getElementById(id).addEventListener('click', event => {
						nextSlide('transcript-certification-addition');
					});
					break;
			}
			break;
		case 'no':
			id = e.srcElement.name;
			document.getElementById(id).removeAttribute('disabled');
			oldElement = document.getElementById(id);
			newElement = oldElement.cloneNode(true);
			oldElement.parentNode.replaceChild(newElement, oldElement);

			switch (id) {
				case 'btn-continue-transcript-question-transcripts':
					document.getElementById(id).addEventListener('click', event => {
						nextSlide('transcript-question-certifications');
					});
					break;
				case 'btn-continue-transcript-question-certification':
					document.getElementById(id).addEventListener('click', event => {
						nextSlide('transcript-end');
					});
					break;
			}
			break;
	}
}

document.querySelectorAll('input[type="radio"]').forEach( element => {
	element.removeEventListener('click', radioButton);
	element.addEventListener('click', radioButton)
});

window.hideSlides = () => {
	let slides = document.querySelectorAll('.slide');
	slides.forEach(slide => {
		slide.classList.remove('fadeInRight');
		slide.classList.remove('fadeInLeft');
	});

	//let btnsContinue = document.querySelectorAll('button.continue-addition');
	//btnsContinue.forEach(btn => {
		//btn.setAttribute('disabled', 'disabled');
	//});
};

window.nextSlide = (componentName) => {
	window.scrollTo(0, 0);
	hideSlides();
	document.getElementById(`component-${componentName}`).classList.add('fadeInRight');

	console.log(componentName);

	if (componentName === 'transcript-end') {
		document.getElementById('card-transcript').classList.add('completed');
		let x = document.getElementById('card-transcript').querySelectorAll('.done__icon');
		x[0].style.display = 'block';
	}

	if (componentName === 'financial-aid-end') {
		document.getElementById('card-financial-aid').classList.add('completed');
		let x = document.getElementById('card-financial-aid').querySelectorAll('.done__icon');
		x[0].style.display = 'block';

		window.paymentsArray = [];
		document.getElementById('set-payments-table').querySelectorAll('.form-control.form-control-sm').forEach((i) => {
			paymentsArray.push(i.value);
		});

		let html = '';

		for (let i = 0; i < paymentsArray.length; i++) {
			html += `<tr>
					  <td width="3%">${i + 1}</td>
					  <td>${paymentsArray[i]}</td>
					</tr>`;
		}

		document.getElementById('how-can-i-pay').innerHTML = html;

	}

	if (componentName === 'start-date-end') {
		window.setStartDate();
		document.getElementById('card-start-date').classList.add('completed');
		let x = document.getElementById('card-start-date').querySelectorAll('.done__icon');
		x[0].style.display = 'block';
	}

	if (componentName === 'program-end') {
		document.getElementById('card-program').classList.add('completed');
		let x = document.getElementById('card-program').querySelectorAll('.done__icon');
		x[0].style.display = 'block';
		document.getElementById('program-placeholder').innerText = document.getElementById('program-table').querySelectorAll('.form-control.form-control-sm')[0].value;
	}

	if (componentName === 'intake-interview-end') {
		document.getElementById('card-intake-interview').classList.add('completed');
		let x = document.getElementById('card-intake-interview').querySelectorAll('.done__icon');
		x[0].style.display = 'block';
	}

	if (componentName === 'readiness-assessment-end') {
		document.getElementById('card-readiness-assessment').classList.add('completed');
		let x = document.getElementById('card-readiness-assessment').querySelectorAll('.done__icon');
		x[0].style.display = 'block';
	}


	if (componentName.indexOf('end') > 0) {
		window.confetti.toggle();
		setTimeout(() => {
			window.confetti.toggle();
		}, 2500);
	}
};

window.previousSlide = (componentName) => {
	window.scrollTo(0, 0);
	hideSlides();
	document.getElementById(`component-${componentName}`).classList.add('fadeInLeft');
};

window.howDoYouFeel = (option, element) => {
	let answer = '';
	let cards = document.querySelectorAll('.card');
	cards.forEach(card => {
		card.classList.remove('completed')
	});

	element.classList.add('completed');

	switch (option) {
		case 1: answer = `Great to hear! Let's get this party started!`; break;
		case 2: answer = `A lot of students feel that way when they begin. Don't worry, we will help you.`; break;
		case 3: answer = `Relax, take a deep breath. We will help you out.`; break;
	}

	let elm = document.getElementById('slide-1-answer');
	elm.setAttribute('style', 'display: block !important');
	elm.innerHTML = answer;

	document.getElementById('slide-1-continue-btn').removeAttribute('disabled');
};

window.changeTheme = (themeName, element) => {
	let el = document.getElementsByTagName('html')[0];
	el.classList.toggle(themeName);
	if (element.innerText === "Halloween On?") {
		element.innerText = "Halloween Off?";
	} else {
		element.innerText = "Halloween On?";
	}
};



window.setPaymentMethod = () => {

};

window.addPayment = () => {
	if (document.getElementById('set-payments-table').children.length < 3) {
		let html = `
			<td>payment option</td>
			<td>
			  <select class="form-control form-control-sm" onchange="setPaymentMethod(this.value)">
				<option value="3P - Third Party">Third Party</option>
				<option value="FA - Financial Aid">Financial Aid</option>
				<option value="SP - Self Pay">Self Pay</option>
				<option value="MTA - Military Tuition Assistance">Military Tuition Assistance</option>
				<option value="VA - Veterans Assistance">Veterans Assistance</option>
				<option value="MVR - Military Vocational Rehab">Military Vocational Rehab</option>
				<option value="WIA - Workforce Investment Act">Workforce Investment Act</option>
				<option value="TAA - Trade Adjustment Act">Trade Adjustment Act</option>
				<option value="Other">Other</option>
				<option value="None">None</option>
			  </select>
			</td>
		  `;

		let child = document.createElement('tr');
		child.innerHTML = html;
		document.getElementById('set-payments-table').appendChild(child);
	}
};

window.removePayment = () => {
	if (document.getElementById('set-payments-table').children.length > 1) {
		let p = document.getElementById('set-payments-table');
		let c = p.lastElementChild;
		p.removeChild(c);
	}
};

window.changeStartDate = (elm) => {
	let month = elm.value.split('-')[0];
	let year = elm.value.split('-')[1];

	document.getElementById('start-date-month').innerHTML = month;
	document.getElementById('start-date-year').innerHTML = year;
	document.getElementById('start-date-placeholder').innerText = elm[elm.selectedIndex].innerText;
};

window.setStartDate = () => {
	document.getElementById('start-date-month').setAttribute('style', 'display: block !important');
	document.getElementById('start-date-year').setAttribute('style', 'display: block !important');
};

new Confetti();