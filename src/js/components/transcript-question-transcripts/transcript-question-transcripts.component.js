import * as html from './transcript-question-transcripts.html';

export default class TranscriptQuestionTranscriptsComponent extends HTMLElement {
	constructor() {
		super();
		//this._root = this.attachShadow({'mode': 'open'});
	}

	connectedCallback() {
		this.innerHTML = html;
	}

	static get observedAttributes() {
		return ['hello'];
	}

	attributeChangedCallback(name, oldValue, newValue) {
		console.log(name, oldValue, newValue);
	}

	disconnectedCallback() {
		console.log('custom element removed from dom');
	}
}