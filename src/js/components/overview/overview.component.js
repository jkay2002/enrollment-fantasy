import * as html from './overview.html';

export default class OverviewComponent extends HTMLElement {
	constructor() {
		super();
		//this._root = this.attachShadow({'mode': 'open'});
	}

	connectedCallback() {
		console.log('connected call back');
		this.innerHTML = html;
	}

	static get observedAttributes() {
		return ['hello'];
	}

	attributeChangedCallback(name, oldValue, newValue) {
		console.log(name, oldValue, newValue);
	}

	disconnectedCallback() {
		console.log('custom element removed from dom');
	}
}