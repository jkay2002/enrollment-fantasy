import * as html from './financial-aid-end.html';

export default class FinancialAidEndComponent extends HTMLElement {
	constructor() {
		super();
		//this._root = this.attachShadow({'mode': 'open'});
	}

	connectedCallback() {
		this.innerHTML = html;
	}

	static get observedAttributes() {
		return ['hello'];
	}

	attributeChangedCallback(name, oldValue, newValue) {
		console.log(name, oldValue, newValue);
	}

	disconnectedCallback() {
		console.log('custom element removed from dom');
	}
}