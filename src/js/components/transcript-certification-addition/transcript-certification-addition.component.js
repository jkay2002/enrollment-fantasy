import * as html from './transcript-certification-addition.html';

export default class TranscriptCertificationAdditionComponent extends HTMLElement {
	constructor() {
		super();
		//this._root = this.attachShadow({'mode': 'open'});
	}

	connectedCallback() {
		this.innerHTML = html;
	}

	static get observedAttributes() {
		return ['hello'];
	}

	attributeChangedCallback(name, oldValue, newValue) {
		console.log(name, oldValue, newValue);
	}

	disconnectedCallback() {
		console.log('custom element removed from dom');
	}
}